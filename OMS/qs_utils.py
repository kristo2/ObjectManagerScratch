from typing import NamedTuple, Callable, Set, TypeVar, Any, Generic

from django.db import models

T = TypeVar('T')
ModelT = TypeVar('ModelT', bound=models.Model)
QuerySetT = TypeVar('QuerySetT', bound='QuerySetInterface')


class QuerySetInterface(models.QuerySet, Generic[ModelT]):
    """ This interface is only used to help PyCharm with completion.

    It does NOT currently work well enough to add completion of ORM params.
    This is a shortcoming of PyCharm: https://youtrack.jetbrains.com/issue/PY-22616
    """
    def filter(self: QuerySetT, *args, **kwargs) -> QuerySetT:
        return super().filter(*args, **kwargs)

    def get(self, *args, **kwargs) -> ModelT:
        return super(QuerySetInterface, self).get(*args, **kwargs)

    def exclude(self: QuerySetT, *args, **kwargs) -> QuerySetT:
        return super().exclude(*args, **kwargs)

    def first(self) -> ModelT:
        return super().first()

    def last(self) -> ModelT:
        return super().last()

    def select_related(self, *fields) -> QuerySetT:
        return super().select_related(*fields)

    def prefetch_related(self, *lookups) -> QuerySetT:
        return super().prefetch_related(*lookups)

    def all(self) -> QuerySetT:
        return super().all()

    def annotate(self, *args, **kwargs) -> QuerySetT:
        return super().annotate(*args, **kwargs)

    def order_by(self, *field_names) -> QuerySetT:
        return super().order_by(*field_names)

    def distinct(self, *field_names) -> QuerySetT:
        return super().distinct(*field_names)


class ActionValidationError(BaseException):
    pass


class ActionCheck(NamedTuple):
    """ The queryset that this action is valid for and the number of excluded instances.
    """
    queryset: models.QuerySet
    excluded: int

    @property
    def is_unchanged(self):
        return self.excluded == 0


def validate_for(validator: Callable[..., ActionCheck], extra_args: Set[str] = None):
    """ A decorator to add on to actions to validate applicability.
    """
    def decorator(action: Callable):
        def wrapped(self: models.QuerySet, *args, **kwargs):
            assert not args, f"Cannot supply args to an action, only kwargs. Supplied: {args}"
            extra_args_values = {
                name: value
                for name, value in kwargs.items()
                if extra_args and name in extra_args
            }
            action_check = validator(self, **extra_args_values)

            if not action_check.is_unchanged:
                count = action_check.queryset.count()
                total = count + action_check.excluded
                raise ActionValidationError(
                    f"Will not apply actions for partial QuerySet.\n"
                    f"- Action: {action.__name__}\n"
                    f"- Validator: {validator.__name__}\n"
                    f"\nThis means that not all targets were valid for this action"
                    f"\nIn this queryset {count} out of {total} instances were valid for this action"
                )
            return action(action_check.queryset, *args, **kwargs)
        return wrapped
    return decorator


def validator(inner_validator: Callable[[T, Any], T]) -> Callable[..., ActionCheck]:
    """ A decorator for validators of actions
    """
    def wrapper(qs: T, *args, **kwargs) -> ActionCheck:
        len_before = qs.count()
        valid_qs = inner_validator(qs, *args, **kwargs)
        len_after = valid_qs.count()
        return ActionCheck(
            queryset=valid_qs,
            excluded=len_before - len_after,
        )
    return wrapper
