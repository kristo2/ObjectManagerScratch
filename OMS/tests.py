from django.test import TestCase
from OMS.models import Product, User, Order, Basket
from OMS.qs_utils import ActionValidationError


class TestSequence(TestCase):
    def test_sequence(self):
        creator = User.objects.create(first_name='God', last_name='Christ', username='YoMama')
        customer = User.objects.create(first_name='Slaava', last_name='Boogu', username='TheBooguestSlaava85')
        w3 = Product.objects.create(
            name='Witcher 3',
            price=60,
            quantity=0,
            creator=creator,
        )
        vodka = Product.objects.create(
            name='Vodka',
            price=5,
            quantity=100,
            creator=creator,
        )

        # New shipment came in and the masses tried to buy too many
        w3_qs = Product.objects.filter(id=w3.id)
        w3_qs.apply_add_stock(quantity=5)
        self.assertRaises(ActionValidationError, lambda: w3_qs.apply_remove_stock(quantity=6))
        assert w3_qs.first().quantity == 5

        # Witcher 3 comes off the shelves midway through a purchase
        w3_qs.update(is_active=False)
        self.assertRaises(ActionValidationError, lambda: w3_qs.apply_remove_stock(quantity=1))
        assert w3_qs.first().quantity == 5

        # Clean up out of stock products
        w3_qs.update(is_active=True)
        w3_qs.apply_remove_stock(quantity=5)  # All gone!
        Product.objects.is_out_of_stock().update(is_active=False)
        w3.refresh_from_db()
        assert w3.is_active is False

        # Operating with an Order
        basket = Basket.objects.create()
        order = Order.objects.create(user=customer, status=Order.STATUS_NEW, basket=basket)
        order_qs = Order.objects.filter(id=order.id)
        self.assertRaises(ActionValidationError, order_qs.accept_order)
