from django.contrib.auth.models import User
from django.db import models
from django.db.models import CASCADE, F, QuerySet, Model

from OMS.qs_utils import validator, validate_for, ActionCheck, QuerySetInterface


class ProductQuerySet(QuerySetInterface['Product']):
    # region Filters
    def is_out_of_stock(self):
        return self.filter(quantity__lt=1)

    def is_in_stock(self):
        return self.filter(quantity__gt=0)

    def is_active(self):
        return self.filter(is_active=True)

    def is_not_in_basket(self):
        return self.filter(baskets=None)
    # endregion

    # region Validators
    @validator
    def can_add_stock(self):
        return self.is_active()

    @validator
    def can_remove_stock(self, quantity: int):
        return self.is_active().is_not_in_basket().filter(quantity__gte=quantity)
    # endregion

    # region Actions
    @validate_for(validator=can_add_stock)
    def apply_add_stock(validated_queryset, *, quantity: int):
        changed = validated_queryset.update(quantity=quantity)
        WishList.objects.apply_notify_change(validated_queryset)
        return changed

    @validate_for(validator=can_remove_stock, extra_args={'quantity'})
    def apply_remove_stock(validated_queryset, *, quantity: int):
        changed = validated_queryset.update(quantity=F('quantity') - quantity)
        WishList.objects.apply_notify_change(validated_queryset)
        return changed
    # endregion


class Product(models.Model):
    name = models.CharField(max_length=64)
    price = models.FloatField()
    quantity = models.IntegerField()

    creator = models.ForeignKey(User, on_delete=CASCADE)
    is_active = models.BooleanField(default=True)

    objects = ProductQuerySet.as_manager()


class Basket(models.Model):
    products = models.ManyToManyField(Product, related_name="baskets")


class OrderQuerySet(QuerySetInterface['Order']):
    def is_new(self):
        return self.filter(status=Order.STATUS_NEW)

    def is_completed(self):
        return self.filter(status=Order.STATUS_COMPLETED)

    def is_cancelled(self):
        return self.filter(status=Order.STATUS_CANCELLED)

    def is_valid_basket(self):
        qs = self.filter(basket__products__isnull=False)
        products = qs.related_products().can_remove_stock(1)
        if products.is_unchanged:
            return qs
        return self.none()

    def related_products(self):
        return Product.objects.filter(baskets__orders__in=self).distinct()

    def related_wishlist(self):
        return WishList.objects.filter(products__baskets__orders__in=self).distinct()

    @validator
    def can_accept_order(queryset) -> ActionCheck:
        return queryset.is_new().is_valid_basket()

    @validate_for(can_accept_order)
    def accept_order(validated_queryset):
        changed = validated_queryset.update(status=Order.STATUS_COMPLETED)
        validated_queryset.related_products().apply_remove_stock(quantity=1)
        validated_queryset.related_wishlist().apply_drop_products(validated_queryset)
        return changed


class Order(models.Model):
    STATUS_NEW = 0
    STATUS_COMPLETED = 1
    STATUS_CANCELLED = 2

    STATUS_CHOICES = (
        ("NEW", STATUS_NEW),
        ("COMPLETED", STATUS_COMPLETED),
        ("CANCELLED", STATUS_CANCELLED),
    )

    user = models.ForeignKey(User, on_delete=CASCADE)
    status = models.PositiveIntegerField(choices=STATUS_CHOICES)
    basket = models.ForeignKey(Basket, on_delete=CASCADE, related_name='orders')

    objects = OrderQuerySet.as_manager()


class WishListQuerySet(QuerySetInterface['WishList']):
    # region Filters
    def is_notifiable(self):
        return self.filter(notifications_on=True, user__is_active=True)

    def is_related_to_products_through(self, products: 'QuerySet[Product]'):
        return self.model.products.through.objects.filter(
            wishlist_id__in=self.values('id'),
            product_id__in=products.values('id'),
        )
    # endregion

    # region Actions
    def apply_notify_change(self, products: 'QuerySet[Product]'):
        qs = self.is_notifiable().filter(products__in=products).distinct()  # type: QuerySet[WishList]
        for wish_list in qs:
            wish_list.notify()
        return len(qs)

    def apply_drop_products(self, products: 'QuerySet[Product]'):
        return self.is_related_to_products_through(products).delete()
    # endregion


class WishList(models.Model):
    user = models.ForeignKey(User, on_delete=CASCADE)
    notifications_on = models.BooleanField(default=True)
    products = models.ManyToManyField(Product)

    objects = WishListQuerySet.as_manager()

    def notify(self):
        print('Notify!')
